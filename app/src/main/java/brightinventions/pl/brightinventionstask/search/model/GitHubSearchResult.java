package brightinventions.pl.brightinventionstask.search.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public final class GitHubSearchResult {
	@JsonProperty("total_count")
	private int totalCount;

	@JsonProperty("incomplete_results")
	private boolean incompleteResults;

	private List<Item> items;

	@Getter
	@ToString
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static final class Item {
		private long id;

		private String name;

		@JsonProperty("full_name")
		private String fullName;

		private Owner owner;

		@JsonProperty("stargazers_count")
		private int stargazersCount;

		@Getter
		@ToString
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static final class Owner {
			@JsonProperty("avatar_url")
			private URI avatarUri;
		}
	}
}
